#!/usr/bin/perl
use 5.020;
use Cwd qw(getcwd);
use autodie qw(chdir);

my $orig = getcwd();

foreach my $lang (glob('*'))
{
    chdir($orig);
    if (-d $lang)
    {
        chdir($lang);
        say $lang;
        foreach my $file (glob('*iPad*'))
        {
            next if -l $file;
            next if $file =~ /ipadPro129/;
            my $number = $file;
            $number =~ s/^(\d+)\D.*/$1/;
            my $newFile = $number . '-' . 'ipadPro129.png';
            next if -e $newFile;
            symlink($file, $newFile);
        }
    }
}
