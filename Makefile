upload:
	cd ..; fastlane deliver --skip_binary_upload true --skip_metadata true --skip_app_version_update true --overwrite_screenshots true --submit_for_review false
prepare:
	perl prepare-ipad.pl
